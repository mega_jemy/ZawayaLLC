<?php

/* themes/robotic/templates/page.html.twig */
class __TwigTemplate_1ac071859579db11b3e040b7ea9502c69d2aaf7f0b10114f5beec257f29d68fe extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'navbar' => array($this, 'block_navbar'),
            'main' => array($this, 'block_main'),
            'header' => array($this, 'block_header'),
            'sidebar_first' => array($this, 'block_sidebar_first'),
            'highlighted' => array($this, 'block_highlighted'),
            'breadcrumb' => array($this, 'block_breadcrumb'),
            'action_links' => array($this, 'block_action_links'),
            'help' => array($this, 'block_help'),
            'content' => array($this, 'block_content'),
            'sidebar_second' => array($this, 'block_sidebar_second'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("set" => 63, "if" => 66, "block" => 67, "include" => 347, "for" => 113);
        $filters = array("raw" => 336, "t" => 82);
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('set', 'if', 'block', 'include', 'for'),
                array('raw', 't'),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 59
        echo " 
";
        // line 61
        echo " 

";
        // line 63
        $context["container"] = (($this->getAttribute($this->getAttribute(($context["theme"] ?? null), "settings", array()), "fluid_container", array())) ? ("container-fluid") : ("container"));
        // line 66
        if (($this->getAttribute(($context["page"] ?? null), "navigation", array()) || $this->getAttribute(($context["page"] ?? null), "navigation_collapsible", array()))) {
            // line 67
            echo "  ";
            $this->displayBlock('navbar', $context, $blocks);
        }
        // line 100
        echo "
";
        // line 104
        echo "
";
        // line 106
        $this->displayBlock('main', $context, $blocks);
        // line 310
        echo "
 <footer class=\"footer\" id=\"footer\">
  ";
        // line 312
        if (((($this->getAttribute(($context["page"] ?? null), "footer_first", array()) || $this->getAttribute(($context["page"] ?? null), "footer_second", array())) || $this->getAttribute(($context["page"] ?? null), "footer_third", array())) || $this->getAttribute(($context["page"] ?? null), "footer_fourth", array()))) {
            // line 313
            echo "    <div id=\"footer-top\" class=\"footer-column\">
      <div class=\"container\">
        <div class=\"row\">
          <div class=\"col-md-3 col-sm-12\">
            ";
            // line 317
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "footer_first", array()), "html", null, true));
            echo "
          </div>
          <div class=\"col-md-3 col-sm-12\">
            ";
            // line 320
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "footer_second", array()), "html", null, true));
            echo "
          </div>
          <div class=\"col-md-3 col-sm-12\">
            ";
            // line 323
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "footer_third", array()), "html", null, true));
            echo "
          </div>
          <div class=\"col-md-3 col-sm-12\">
            ";
            // line 326
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "footer_fourth", array()), "html", null, true));
            echo "
          </div>
        </div>
      </div>
    </div>
  ";
        }
        // line 332
        echo "
  ";
        // line 333
        if ($this->getAttribute(($context["page"] ?? null), "footer", array())) {
            // line 334
            echo "    <div id=\"footer-bottom\">
      <div class=\"container\">
        ";
            // line 336
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(($context["social_links"] ?? null)));
            echo "
      </div>
    ";
            // line 338
            $this->displayBlock('footer', $context, $blocks);
            // line 343
            echo "    </div>
  ";
        }
        // line 345
        echo "</footer>

";
        // line 347
        $this->loadTemplate((($context["directory"] ?? null) . "/templates/style.html.twig"), "themes/robotic/templates/page.html.twig", 347)->display($context);
        // line 348
        echo "


";
    }

    // line 67
    public function block_navbar($context, array $blocks = array())
    {
        // line 68
        echo "    ";
        // line 69
        $context["navbar_classes"] = array(0 => "navbar", 1 => (($this->getAttribute($this->getAttribute(        // line 71
($context["theme"] ?? null), "settings", array()), "navbar_inverse", array())) ? ("navbar-inverse") : ("navbar-default")), 2 => "navbar-fixed-top");
        // line 75
        echo "    <header";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["attributes"] ?? null), "addClass", array(0 => ($context["navbar_classes"] ?? null)), "method"), "html", null, true));
        echo " id=\"navbar\" role=\"banner\">
      <div class=\"container\">
        <div class=\"navbar-header\">
          ";
        // line 78
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "navigation", array()), "html", null, true));
        echo "
          ";
        // line 80
        echo "          ";
        if ($this->getAttribute(($context["page"] ?? null), "navigation_collapsible", array())) {
            // line 81
            echo "            <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
              <span class=\"sr-only\">";
            // line 82
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Toggle navigation")));
            echo "</span>
              <span class=\"icon-bar\"></span>
              <span class=\"icon-bar\"></span>
              <span class=\"icon-bar\"></span>
            </button>
          ";
        }
        // line 88
        echo "        </div>

        ";
        // line 91
        echo "        ";
        if ($this->getAttribute(($context["page"] ?? null), "navigation_collapsible", array())) {
            // line 92
            echo "          <div class=\"navbar-collapse collapse\">
            ";
            // line 93
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "navigation_collapsible", array()), "html", null, true));
            echo "
          </div>
        ";
        }
        // line 96
        echo "      </div>
    </header>
  ";
    }

    // line 106
    public function block_main($context, array $blocks = array())
    {
        // line 107
        echo "
";
        // line 108
        if ((($context["is_front"] ?? null) && ($context["is_slideshow"] ?? null))) {
            // line 109
            echo "
  <div id=\"roboticCarousel\" class=\"carousel slide\" data-ride=\"carousel\" data-interval=\"7000\">
    <!-- Indicators -->
    <ol class=\"carousel-indicators\">
      ";
            // line 113
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["slider_number"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["slider_numbers"]) {
                // line 114
                echo "        ";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($context["slider_numbers"]));
                echo "
      ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['slider_numbers'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 116
            echo "    </ol>

    <!-- Wrapper for slides -->
    <div class=\"carousel-inner\" role=\"listbox\">

      ";
            // line 121
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["slider_content"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["slider_contents"]) {
                // line 122
                echo "        ";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($context["slider_contents"]));
                echo "
      ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['slider_contents'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 124
            echo "        </div>

    <!-- Left and right controls -->
    <a class=\"left carousel-control\" href=\"#roboticCarousel\" role=\"button\" data-slide=\"prev\">
      <span class=\"glyphicon glyphicon-chevron-left\" aria-hidden=\"true\"></span>
      <span class=\"sr-only\">Previous</span>
    </a>
    <a class=\"right carousel-control\" href=\"#roboticCarousel\" role=\"button\" data-slide=\"next\">
      <span class=\"glyphicon glyphicon-chevron-right\" aria-hidden=\"true\"></span>
      <span class=\"sr-only\">Next</span>
    </a>
  </div>

";
        }
        // line 138
        echo "


    ";
        // line 142
        echo "    ";
        if (($context["is_front"] ?? null)) {
            // line 143
            echo "
      ";
            // line 144
            if ($this->getAttribute(($context["page"] ?? null), "home_feature", array())) {
                // line 145
                echo "        <div role=\"main\" class=\"feature\">
          <div class=\"";
                // line 146
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["container"] ?? null), "html", null, true));
                echo " js-quickedit-main-content\">
            <div class=\"row\">
              <div class=\"col-sm-12 home-container\" role=\"home-heading\">
              ";
                // line 149
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "home_feature", array()), "html", null, true));
                echo "
              </div>
            </div>
          </div>
        </div>
      ";
            }
            // line 155
            echo "
      ";
            // line 156
            if ($this->getAttribute(($context["page"] ?? null), "home_about", array())) {
                // line 157
                echo "        <div role=\"main\" class=\"about\">
          <div class=\"";
                // line 158
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["container"] ?? null), "html", null, true));
                echo " js-quickedit-main-content\">
            <div class=\"row\">
              <div class=\"col-sm-12 home-container\" role=\"home-heading\">
              ";
                // line 161
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "home_about", array()), "html", null, true));
                echo "
              </div>
            </div>
          </div>
        </div>
      ";
            }
            // line 167
            echo "
      ";
            // line 168
            if ($this->getAttribute(($context["page"] ?? null), "home_team", array())) {
                // line 169
                echo "        <div role=\"main\" class=\"team partner\">
          <div class=\"";
                // line 170
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["container"] ?? null), "html", null, true));
                echo " js-quickedit-main-content\">
            <div class=\"row\">
              <div class=\"col-sm-12 home-container\" role=\"home-heading\">
              ";
                // line 173
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "home_team", array()), "html", null, true));
                echo "
              </div>
            </div>
          </div>
        </div>
      ";
            }
            // line 179
            echo "
      ";
            // line 180
            if ($this->getAttribute(($context["page"] ?? null), "home_testimonial", array())) {
                // line 181
                echo "        <div role=\"main\" class=\"testimonial\">
          <div class=\"";
                // line 182
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["container"] ?? null), "html", null, true));
                echo " js-quickedit-main-content\">
            <div class=\"row\">
              <div class=\"col-sm-12 home-container\" role=\"home-heading\">
              ";
                // line 185
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "home_testimonial", array()), "html", null, true));
                echo "
              </div>
            </div>
          </div>
        </div>
      ";
            }
            // line 191
            echo "
    ";
        }
        // line 193
        echo "

  



  <div role=\"main\" class=\"main-container\">
    <div class=\"";
        // line 200
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["container"] ?? null), "html", null, true));
        echo " js-quickedit-main-content\">
      <div class=\"row\">

        ";
        // line 204
        echo "        ";
        if ($this->getAttribute(($context["page"] ?? null), "header", array())) {
            // line 205
            echo "          ";
            $this->displayBlock('header', $context, $blocks);
            // line 210
            echo "        ";
        }
        // line 211
        echo "        

        ";
        // line 214
        echo "        
        ";
        // line 215
        if ( !($context["is_front"] ?? null)) {
            // line 216
            echo "          ";
            // line 217
            echo "          ";
            // line 218
            $context["content_classes"] = array(0 => ((($this->getAttribute(            // line 219
($context["page"] ?? null), "sidebar_first", array()) && $this->getAttribute(($context["page"] ?? null), "sidebar_second", array()))) ? ("col-sm-6") : ("")), 1 => ((($this->getAttribute(            // line 220
($context["page"] ?? null), "sidebar_first", array()) && twig_test_empty($this->getAttribute(($context["page"] ?? null), "sidebar_second", array())))) ? ("col-sm-9") : ("")), 2 => ((($this->getAttribute(            // line 221
($context["page"] ?? null), "sidebar_second", array()) && twig_test_empty($this->getAttribute(($context["page"] ?? null), "sidebar_first", array())))) ? ("col-sm-9") : ("")), 3 => (((twig_test_empty($this->getAttribute(            // line 222
($context["page"] ?? null), "sidebar_first", array())) && twig_test_empty($this->getAttribute(($context["page"] ?? null), "sidebar_second", array())))) ? ("col-sm-12") : ("")));
            // line 225
            echo "
        ";
        } else {
            // line 227
            echo "
          ";
            // line 228
            $context["content_classes"] = array(0 => "col-md-12");
            // line 229
            echo "
        ";
        }
        // line 231
        echo "
        
        ";
        // line 234
        echo "        ";
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_first", array())) {
            // line 235
            echo "          ";
            $this->displayBlock('sidebar_first', $context, $blocks);
            // line 240
            echo "        ";
        }
        // line 241
        echo "

        <section";
        // line 243
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content_attributes"] ?? null), "addClass", array(0 => ($context["content_classes"] ?? null)), "method"), "html", null, true));
        echo ">

          ";
        // line 246
        echo "          ";
        if ($this->getAttribute(($context["page"] ?? null), "highlighted", array())) {
            // line 247
            echo "            ";
            $this->displayBlock('highlighted', $context, $blocks);
            // line 250
            echo "          ";
        }
        // line 251
        echo "
          ";
        // line 253
        echo "          ";
        if ($this->getAttribute(($context["page"] ?? null), "breadcrumb", array())) {
            // line 254
            echo "            ";
            $this->displayBlock('breadcrumb', $context, $blocks);
            // line 257
            echo "          ";
        }
        // line 258
        echo "
          ";
        // line 260
        echo "          ";
        if (($context["action_links"] ?? null)) {
            // line 261
            echo "            ";
            $this->displayBlock('action_links', $context, $blocks);
            // line 264
            echo "          ";
        }
        // line 265
        echo "
          ";
        // line 267
        echo "          ";
        if ($this->getAttribute(($context["page"] ?? null), "help", array())) {
            // line 268
            echo "            ";
            $this->displayBlock('help', $context, $blocks);
            // line 271
            echo "          ";
        }
        // line 272
        echo "
          ";
        // line 274
        echo "          ";
        $this->displayBlock('content', $context, $blocks);
        // line 278
        echo "        </section>

        ";
        // line 281
        echo "        ";
        if ( !($context["is_front"] ?? null)) {
            // line 282
            echo "          ";
            if ($this->getAttribute(($context["page"] ?? null), "sidebar_second", array())) {
                // line 283
                echo "            ";
                $this->displayBlock('sidebar_second', $context, $blocks);
                // line 288
                echo "          ";
            }
            // line 289
            echo "        ";
        }
        // line 290
        echo "
        <div class=\"container\" id=\"featured\">
          <div class=\"row\">
            <div class=\"col-md-4 col-sm-12\">
              ";
        // line 294
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "featured_bottom_first", array()), "html", null, true));
        echo "
            </div>
            <div class=\"col-md-4 col-sm-12\">
              ";
        // line 297
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "featured_bottom_second", array()), "html", null, true));
        echo "
            </div>
            <div class=\"col-md-4 col-sm-12\">
              ";
        // line 300
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "featured_bottom_third", array()), "html", null, true));
        echo "
            </div>
          </div>
        </div>


      </div>
    </div>
  </div>
";
    }

    // line 205
    public function block_header($context, array $blocks = array())
    {
        // line 206
        echo "            <div class=\"col-sm-12\" role=\"heading\">
              ";
        // line 207
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "header", array()), "html", null, true));
        echo "
            </div>
          ";
    }

    // line 235
    public function block_sidebar_first($context, array $blocks = array())
    {
        // line 236
        echo "            <aside class=\"col-md-3\" role=\"complementary\">
              ";
        // line 237
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "sidebar_first", array()), "html", null, true));
        echo "
            </aside>
          ";
    }

    // line 247
    public function block_highlighted($context, array $blocks = array())
    {
        // line 248
        echo "              <div class=\"highlighted\">";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "highlighted", array()), "html", null, true));
        echo "</div>
            ";
    }

    // line 254
    public function block_breadcrumb($context, array $blocks = array())
    {
        // line 255
        echo "              ";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "breadcrumb", array()), "html", null, true));
        echo "
            ";
    }

    // line 261
    public function block_action_links($context, array $blocks = array())
    {
        // line 262
        echo "              <ul class=\"action-links\">";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["action_links"] ?? null), "html", null, true));
        echo "</ul>
            ";
    }

    // line 268
    public function block_help($context, array $blocks = array())
    {
        // line 269
        echo "              ";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "help", array()), "html", null, true));
        echo "
            ";
    }

    // line 274
    public function block_content($context, array $blocks = array())
    {
        // line 275
        echo "            <a id=\"main-content\"></a>
            ";
        // line 276
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "content", array()), "html", null, true));
        echo "
          ";
    }

    // line 283
    public function block_sidebar_second($context, array $blocks = array())
    {
        // line 284
        echo "              <aside class=\"col-md-3\" role=\"complementary\">
                ";
        // line 285
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "sidebar_second", array()), "html", null, true));
        echo "
              </aside>
            ";
    }

    // line 338
    public function block_footer($context, array $blocks = array())
    {
        // line 339
        echo "        <div class=\"";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["container"] ?? null), "html", null, true));
        echo "\" role=\"contentinfo\">
        ";
        // line 340
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "footer", array()), "html", null, true));
        echo "
        </div></div>
    ";
    }

    public function getTemplateName()
    {
        return "themes/robotic/templates/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  651 => 340,  646 => 339,  643 => 338,  636 => 285,  633 => 284,  630 => 283,  624 => 276,  621 => 275,  618 => 274,  611 => 269,  608 => 268,  601 => 262,  598 => 261,  591 => 255,  588 => 254,  581 => 248,  578 => 247,  571 => 237,  568 => 236,  565 => 235,  558 => 207,  555 => 206,  552 => 205,  538 => 300,  532 => 297,  526 => 294,  520 => 290,  517 => 289,  514 => 288,  511 => 283,  508 => 282,  505 => 281,  501 => 278,  498 => 274,  495 => 272,  492 => 271,  489 => 268,  486 => 267,  483 => 265,  480 => 264,  477 => 261,  474 => 260,  471 => 258,  468 => 257,  465 => 254,  462 => 253,  459 => 251,  456 => 250,  453 => 247,  450 => 246,  445 => 243,  441 => 241,  438 => 240,  435 => 235,  432 => 234,  428 => 231,  424 => 229,  422 => 228,  419 => 227,  415 => 225,  413 => 222,  412 => 221,  411 => 220,  410 => 219,  409 => 218,  407 => 217,  405 => 216,  403 => 215,  400 => 214,  396 => 211,  393 => 210,  390 => 205,  387 => 204,  381 => 200,  372 => 193,  368 => 191,  359 => 185,  353 => 182,  350 => 181,  348 => 180,  345 => 179,  336 => 173,  330 => 170,  327 => 169,  325 => 168,  322 => 167,  313 => 161,  307 => 158,  304 => 157,  302 => 156,  299 => 155,  290 => 149,  284 => 146,  281 => 145,  279 => 144,  276 => 143,  273 => 142,  268 => 138,  252 => 124,  243 => 122,  239 => 121,  232 => 116,  223 => 114,  219 => 113,  213 => 109,  211 => 108,  208 => 107,  205 => 106,  199 => 96,  193 => 93,  190 => 92,  187 => 91,  183 => 88,  174 => 82,  171 => 81,  168 => 80,  164 => 78,  157 => 75,  155 => 71,  154 => 69,  152 => 68,  149 => 67,  142 => 348,  140 => 347,  136 => 345,  132 => 343,  130 => 338,  125 => 336,  121 => 334,  119 => 333,  116 => 332,  107 => 326,  101 => 323,  95 => 320,  89 => 317,  83 => 313,  81 => 312,  77 => 310,  75 => 106,  72 => 104,  69 => 100,  65 => 67,  63 => 66,  61 => 63,  57 => 61,  54 => 59,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "themes/robotic/templates/page.html.twig", "C:\\Users\\MeGa\\Sites\\devdesktop\\zawayaTask\\themes\\robotic\\templates\\page.html.twig");
    }
}
