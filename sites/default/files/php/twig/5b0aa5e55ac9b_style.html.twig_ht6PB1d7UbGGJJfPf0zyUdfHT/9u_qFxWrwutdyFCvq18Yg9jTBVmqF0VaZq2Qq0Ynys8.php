<?php

/* themes/robotic/templates/style.html.twig */
class __TwigTemplate_e8f96d03c9d733488345e3c068189a3c3a82dc0a409a01b1a1781fb17a448b44 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array();
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array(),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 1
        echo "<style>
  body {
      background-color: ";
        // line 3
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, (($this->getAttribute(($context["robotic_colors"] ?? null), "body_bg", array(), "any", true, true)) ? ($this->getAttribute(($context["robotic_colors"] ?? null), "body_bg", array())) : ("#ffffff")), "html", null, true));
        echo ";
  }
  a.post-details,
  header#navbar{
    background-color: ";
        // line 7
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["robotic_colors"] ?? null), "header_bg", array()), "html", null, true));
        echo " ;
  }
  a.post-details,
  .social-section .social-links i.fa,
  #navbar .nav>li>a {
    color: ";
        // line 12
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["robotic_colors"] ?? null), "header_font", array()), "html", null, true));
        echo " ;
  }
  a.post-details,
  #navbar .nav>li>a:hover{
    color: ";
        // line 16
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["robotic_colors"] ?? null), "header_font", array()), "html", null, true));
        echo " !important;
    opacity: 0.7;
  }
  .social-section .social-links i.fa,
  .btn-btt{
    background-color: ";
        // line 21
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["robotic_colors"] ?? null), "header_bg", array()), "html", null, true));
        echo " ;
    color: ";
        // line 22
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["robotic_colors"] ?? null), "header_font", array()), "html", null, true));
        echo " ;
  }
</style>";
    }

    public function getTemplateName()
    {
        return "themes/robotic/templates/style.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  81 => 22,  77 => 21,  69 => 16,  62 => 12,  54 => 7,  47 => 3,  43 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "themes/robotic/templates/style.html.twig", "C:\\Users\\MeGa\\Sites\\devdesktop\\zawayaTask\\themes\\robotic\\templates\\style.html.twig");
    }
}
